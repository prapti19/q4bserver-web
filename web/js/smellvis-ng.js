var local_webservice_url_base = 'http://localhost:8080/analysis-service?URL=';
var remote_webservice_url_base = 'https://scratchanalyzer.appspot.com/analysis-service?URL=';
var webservice_url_base = remote_webservice_url_base;

var app = angular.module('analysis-result',['ngMaterial','ui.bootstrap']);
// avoid the conflict with Go's double braces, Hugo's underlying html processing, 
app.config(function ($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

// scrolling
app.run(['$anchorScroll', function ($anchorScroll) {
    $anchorScroll.yOffset = 50;   // always scroll by 50 extra pixels
}]);

// Create the factory that share the Fact
app.factory('$selection', function(){
  var data = {
    selected :''
  };
  return { 
        getSelected: function () {
            return data.selected;
        },
        setSelected: function (x) {
            data.selected = x;
          }
      };
});

app.controller('renderCtrl', function($scope, $http, $anchorScroll, $location, $selection) {
      //handling analysis request
      $scope.project_url = ""; //https://scratch.mit.edu/projects/169526205/
      $scope.idSelected ="";
      $scope.isLoading = false;
      $scope.analyze = function (){
        $scope.isLoading = true;
        $http({
          method: 'GET',
          url: webservice_url_base + $scope.project_url
        }).then(function successCallback(response) {
            
            $scope.SmellsPerSprite = response.data.SmellsPerSprite;
            $scope.Others = response.data.Others;
            // console.info(response.data);
            // this callback will be called asynchronously
            // when the response is available


            $scope.isLoading = false;
          }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
      };

      //scrolling

      $scope.gotoAnchor = function () {
          var var0 = arguments[0];
          var var1 = arguments[1];


        var newHash = '';
        if(var1!=null){
          newHash = 'anchor'+var0 + var1;
        } else{
          newHash = 'anchor'+var0;
        }
        
        if(var1 == 'Uncommunicative Name'){
          newHash = 'anchor'+var0;
        }

        // var newHash = 'anchor' + sprite+smell;
        if ($location.hash() !== newHash) {
          // set the $location.hash to `newHash` and
          // $anchorScroll will automatically scroll to it
          $location.hash(newHash);

        } else {
          // call $anchorScroll() explicitly,
          // since $location.hash hasn't changed
          $anchorScroll();
        }

      };

      
       $scope.formatBlockSeq = function(blocks){
         var result = "";
         for(var i=0; i<blocks.length;i++){
           result += blocks[i];
           result += "\n";
         }
         return result;
       };

       $scope.render = function(script){
         var svg = scratchblocks(script);
       };


       //selection
      $scope.$watch('idSelected', function (newValue, oldValue) {
        if (newValue !== oldValue) $selection.setSelected(newValue);
      });

      $scope.$watch(function () { return $selection.getSelected(); }, function (newValue, oldValue) {
        if (newValue !== oldValue) $scope.idSelected = newValue;
      });
      

      $scope.select = function () {
        var var0 = arguments[0];
        var var1 = arguments[1];

        // UN smell
        if(var1 == 'Uncommunicative Name'){
          $scope.idSelected = var0; //point to sprite name
          return;
        }

        if(var1!=null){
          $scope.idSelected = var0+var1;
        } else{
          $scope.idSelected = var0;
        }
        
      }


});


// block-rendering directive
app.directive("svg",function(){
 return {
   link: function (scope, iElement, iAttrs) {
     // var svg = angular.element('<svg width="600" height="100" class="svg"></svg>');
     script = "move (10) steps";
     var svg = scratchblocks(iAttrs.scratchblocks);
     iElement.append(svg);
   }
 }
});

// // visjs rendering directive
app.directive("visjs", function(){
  var options = {
                  autoResize: true,
                    height: '100%',
                    width: '100%',    
                  layout:{randomSeed:4}, 
                  interaction:{
                    hover: true,
                    dragNodes: true,
                    dragView: false,
                    zoomView: false
                  },
                  physics:{
                            barnesHut: {
                            springLength: 200
                          }
                  }
                }; 
  return {
    link: function (scope, element, attr) {
      var json_data = scope.$eval(attr.data);
      if(json_data){
        var nodes = new vis.DataSet(json_data.nodes);
        var edges = new vis.DataSet(json_data.edges);
        var data = {
          nodes: nodes,
          edges: edges
        };


        var network = new vis.Network(element.context, data, options);

        snippets = [];
        previous = null;
        network.on("select", function (properties) {
          var selected_obj_id = null;
          var selected_obj = null;

          if(properties.edges.length!=0){
            selected_obj_id = properties.edges[0];
            selected_obj = edges.get(selected_obj_id);
          }

          if(properties.nodes.length!=0){
            selected_obj_id = properties.nodes[0];
            selected_obj = nodes.get(selected_obj_id);
          }

          dom_el = element.context; 
          snippet_doms = dom_el.querySelectorAll('svg');
          snippet_doms.forEach(function(el){
            dom_el.removeChild(el);
          });
          
          var svg = scratchblocks(selected_obj.snippets[0]);
          element.append(svg);
           
        });


      }
    }
  }
});

